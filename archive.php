<?php get_header(); ?>
<article id="article" class="col-sm-8 col-xs-12">
		<?php if (have_posts()) : ?>

 			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

			<?php /* If this is a category archive */ if (is_category()) { ?>
				<h2>Archivo para la Categoria &#8216;<?php single_cat_title(); ?>&#8217;</h2>

			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<h2>Etiquetas de Entradaa &#8216;<?php single_tag_title(); ?>&#8217;</h2>

			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<h2>Archivos para <?php the_time('F jS, Y'); ?></h2>

			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<h2>Archivo para <?php the_time('F, Y'); ?></h2>

			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<h2>Archivo para <?php the_time('Y'); ?></h2>

			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<h2>Archivo de Autor</h2>

			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h2>Archivos de Blog</h2>
			
			<?php } ?>

			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

			<?php while (have_posts()) : the_post(); ?>
			
				<div <?php post_class() ?>>
				
					<h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
					
					<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

					<div class="entry">
						<?php the_content('Leer mas...'); ?>
					</div>
					</br>
					<a href="https://www.facebook.com/stepintofashionblog" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/facebook-icon.png" alt="" /></a>
				  <a href="http://instagram.com/stepintofashionblog" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/instagram-icon.png"  alt=""  /></a>
				  <a href="" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/pinterest-icon.png"  alt=""  /></a>
					</br></br>
					<hr>
				</div>

			<?php endwhile; ?>

			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
			
	<?php else : ?>

		<h2>Nada Encontrado</h2>

	<?php endif; ?>
</article>
	</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>