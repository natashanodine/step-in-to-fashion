<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Natasha Nodine Wyatt">
	
	<link rel="shortcut icon" href="/favicon.ico">

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <script src="js/respond.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>	
	<link href='http://fonts.googleapis.com/css?family=Marck+Script' rel='stylesheet' type='text/css'>
	


	<link href='http://fonts.googleapis.com/css?family=Alex+Brush' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Pinyon+Script' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>

	
	
	
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="outer" id="page-wrap">
	<div class="container" >
		 <header id="header" class="text-center row">
			<a href="http://stepintofashion.net/"><h1>Step into Fashion</h1></a>
			<p>por Paula Terrazo</p>
			 <div class="header_icons">
				  <a href="https://www.facebook.com/stepintofashionblog" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/facebook-icon.png" alt="" /></a>
				  <a href="http://instagram.com/stepintofashionblog" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/instagram-icon.png"  alt=""  /></a>
				  <a href="" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/pinterest-icon.png"  alt=""  /></a>
			 </div>
		</header>
		<hr />
		<div id="main_content">