<?php get_header(); ?>
<article id="article" class="col-sm-8 col-xs-12">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			
			<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
			
			<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

			<div class="entry">
				<?php the_content(' Leer mas...'); ?>
			</div>

			<div class="postmetadata">
				
				<?php the_category(', ') ?> | 
				<?php comments_popup_link('Sin Comentarios &#187;', '1 Comentario &#187;', '% Comentarios &#187;'); ?>
				</br>
				<a href="https://www.facebook.com/stepintofashionblog" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/facebook-icon.png" alt="" /></a>
				  <a href="http://instagram.com/stepintofashionblog" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/instagram-icon.png"  alt=""  /></a>
				  <a href="" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/pinterest-icon.png"  alt=""  /></a>
				</br>
			</div>
		<hr>
		</div>
	
	<?php endwhile; ?>

	<?php //include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>
	</br>
	
</article>
</div><!-- #main_content-->
<?php get_sidebar(); ?>

<?php get_footer(); ?>