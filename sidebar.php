<aside id="aside" class="col-sm-4 col-xs-12">
    <h3 class="text-center">Fashion and Beauty Blogger</h3>
    <img src="http://vilcabamba-hotel.com/stepintofashion/wp-content/uploads/2019/08/about1.png" alt="" class="img-about img-responsive img-circle center-block"/>
    <p id="about">
Comencé este proyecto porque las tendencias de moda son temas que siempre me han gustado. Esto es algo que me apasiona y me encantaría compartirlo. Ojalá me acompañen en esta aventura!!</br></br>
I started this project because I love all fashion topics. This is my passion and I love to share it. I hope you join me in this adventure!!</br>

</p>
<hr>
	
	<?php get_search_form(); ?>
	
	<h3>Entradas Recientes</h3>
		<ul>
			<?php wp_get_archives( array( 'type' => 'postbypost', 'limit' => 10, 'format' => 'html' ) ); ?>
		</ul>
		
    	<h3>Archivos</h3>
    	<ul>
    		<?php wp_get_archives('type=monthly'); ?>
    	</ul>
		
		<h3>Categorias</h3>
        <ul>
    	   <?php wp_list_categories('show_count=1&title_li='); ?>
        </ul>
		
    <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar Widgets')) : else : ?>
    
        <!-- All this stuff in here only shows up if you DON'T have any widgets active in this zone -->
		
	<?php endif; ?>
	
</aside>