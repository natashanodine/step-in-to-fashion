<?php get_header(); ?>
<article id="article" class="col-sm-8 col-xs-12">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			
			<h2><?php the_title(); ?></h2>
			
			<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

			<div class="entry">
				
				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
				
				

			</div>
				<a href="https://www.facebook.com/stepintofashionblog" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/facebook-icon.png" alt="" /></a>
				  <a href="http://instagram.com/stepintofashionblog" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/instagram-icon.png"  alt=""  /></a>
				  <a href="" target="blank"><img src="http://stepintofashion.net/wp-content/uploads/2014/12/pinterest-icon.png"  alt=""  /></a>
			</br></br>
			<?php edit_post_link('Editar esta entrada','','.'); ?>
			</br>
		</div>

	<div id="comments"><?php comments_template(); ?></div>
	
	<?php endwhile; endif; ?>
<article>	
</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>